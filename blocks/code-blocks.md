[TOC]

Список всех блоков, которые только могут понадобится кодеру

# Основные блоки кода и что они делают
## Блоки-события
Начало любой программы - то что приводит ее в действие

Эти блоки запускают свою линию кода в ответ на различные события, например, дейсвтия игрока, сообщения в чате, события мобов и.т.д.

![](https://static.wikia.nocookie.net/minecraft/images/8/89/BlockOfDiamondNew.png){width=30px} [события игрока](blocks/player-events.md) - реакция на действия игрока.

![](https://static.wikia.nocookie.net/minecraft/images/0/01/RedstoneBlockNew.png){width=30px} [события мира](blocks/world-events.md) - реакция на что-то что происходит в мире.

![](https://static.wikia.nocookie.net/minecraft/images/9/9d/BlockOfGoldNew.png){width=30px} [события сущностей](blocks/entity-events.md)  - реакция на действия мобов/сущностей.